# Start the original web controller

Clone Joe's original repo (https://github.com/ashleyfraser19/SSI-platform-development) and follow the instructions to download and install the von network.

The **new** web controller code in this repository contains an initialisation bug meaning the Endorser doesn't properly initialise. This needs fixing in general, but a temporary fix is to run the original web controller which performs setup correctly:

```
    cd ../SSI-platform-development-main/webDemo/webcontroller
    npm start
```
Make sure to run the original web controller first because it will default to port 3000, which is what it expects to run on.

# For a clean start, clear all docker instances and restart them
```
        sudo docker ps -a               # lists all docker instances

        cd von-network
        sudo ./manage stop              # stop the von network

        cd ../SSI-platform-development-main/webDemo/cloudAgents/
        sudo ./manage all stop          # stop the docker cloud agents

        sudo docker container prune     # prune all the agents
        sudo docker ps -a               # should show no docker instances
```
Run the von-network and ACA-Py cloud agent docker instances
```
        cd von-network
        sudo ./manage start             # start the von network docker instance

        cd ../SSI-platform-development-main/webDemo/cloudAgents
        sudo ./manage all start         # start the docker cloud agents

        sudo docker ps -a               # should show the running docker instances
```
Start the file server
```
        cd ../SSI-platform-development-main/webDemo/file-server
        npm start                       # you may want to clear the /uploads and /imageRegistry folders
```
# Start the new web controllers (Holder-App and all Agents)
```
    cd Holder-App
    npm start                           # start Ruths holder app

    cd Portal-Apps/IV-Portal-Cavendish/
    npm start                           # answer Y to run on a different port

    cd Portal-Apps/IV-Portal-Thoday/
    npm start                           # answer Y to run on a different port

    cd Portal-Apps/IV-Portal-Franklin/
    npm start                           # answer Y to run on a different port
```
Of the four windows that appear I recommend splitting them into two browsers, the left for the Holder-App and the right for the remaining three agents so that you may easily drag over QR codes.

Every time you clear the docker instances, you must currently copy the newly generated certificates for Cavendish and Thoday over to the `ssi-lib` shared library. You can find these JSON objects outputted to the developer console for Cavendish and Thoday on first running those web apps. Copy these into the below file.
```
        /ssi-lib/src/Common.js
```
Here's an example from Cavendish where the `schema_id`, `cred_def_id`, `issuer_id` and `schema_issuer_id` change every time the von network is cleared and restarted.
```
        {
            "schema_name": "PhotographerCertification",
            "schema_version": "1.0",
            "schema_id": "PxNTs3yPSL3SD9e7DpAqF2:2:PhotographerCertification:1.0",
            "cred_def_id": "PxNTs3yPSL3SD9e7DpAqF2:3:CL:170:default",
            "issuer_did": "PxNTs3yPSL3SD9e7DpAqF2",
            "schema_issuer_did": "PxNTs3yPSL3SD9e7DpAqF2"
        }
```
# Operation

The apps are self-explanatory, just drag and drop the QR code from Cavendish over to the Holder-App Home page, click continue and follow the instructions. Same goes for Thoday. The Franklin flow is slightly different in that you start the process from the Franklin side before dragging and dropping the QR code to the Holder-App. For a live demo see `demo.mp4` in this repo.

# Caveats & Tips

1. If you `npm install some-library` to the agents, you may have to `npm link ssi-lib` again for that agent to relink the shared `ssi-lib` (you may need to back-track from your current directory, e.g. `npm link ../ssi-lib` etc.

2. Not very common, but sometimes the docker instances can fail to initialise. You can run `sudo docker logs INSTANCE_ID` to debug this; the web-socket relay dockers are usually the cause.

3. Resetting von network can be handy after many hours of development due to the large number of credentials accumulated in the wallet.

4. Don't forget to copy the Cavendish and Thoday certificates over to `/ssi-lib/src/Common.js` whenever you reset the von network dockers. Also note that sometimes the certificates to copy do not appear in the Cavendish or Thoday developer outputs. In such cases, just restart Joe's webcontroller and try again, which seemed to work well for me.

5. Check out `package.json` where you can find the script to run the Holder-App in an Electron container.
   
6. Make sure to use Node 16 (`npx install 16.x.y`) as I ran into difficulties with the web controller using a later version.
