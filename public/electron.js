const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const dialog = electron.dialog;
const gotTheLock = app.requestSingleInstanceLock();
const path = require('path');
const url = require('url');

app.removeAsDefaultProtocolClient('decade-ssi');

let mainWindow = null;

if (!gotTheLock) {
  app.quit();
} else {

  app.on('second-instance', (event, args) => {
    if (mainWindow) {
      // TODO: Send to requested page too?
      sendToWindow(args);
    }
  });

  app.on('ready', createWindow);
}

function createWindow () {
  mainWindow = new BrowserWindow({width: 800, height: 800, autoHideMenuBar: true})
  mainWindow.webContents.on('new-window', (event, url) => {
    event.preventDefault();
    mainWindow.loadURL(url);
  });

  updateWindow();
  sendToWindow(process.argv);
}

function updateWindow() {
  if (mainWindow.isMinimized()) { mainWindow.restore(); }
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  mainWindow.focus();
}

function sendToWindow(s) {
    if (mainWindow && mainWindow.webContents) {
        mainWindow.webContents.executeJavaScript(`alert("${s}")`)

    }
}
