/*

    *** NOTE ***

    The original webcontroller makes use of a few Node modules.
    Node usually runs on a server behind a private API and grants access to the servers' file system.

    This is fine for a server, but... Web apps shouldn't be able to access a local file system (imagine the carnage !)

    For that reason I'm going to try to avoid using those modules requiring local file access,
    which means that some things (e.g. strings, urls etc) will have to appear in code for the time being,
    e.g. ".env" variables etc (below)

    Eventually these would sit in the .env file on the server, or be fetched from a server by the client
    But for now we can just avoid system file access and hard code the various strings / urls / constants.

*/

export const REACT_APP_RUTH_API = 'http://localhost:8421/'
export const REACT_APP_RUTH_HOOK = 'ws://localhost:8420/socket/'
export const FILE_SERVER_URL = "http://localhost:5000/"
