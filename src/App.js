import './App.css';
import { React, useEffect, useState } from "react";

import * as EnvVars from './env/env';
import * as SSIConnections from 'ssi-lib/Connections';
import * as SSICredentialExchange from 'ssi-lib/CredentialExchange';
import * as SSICredentialHelper from 'ssi-lib/CredentialHelper';
import * as SSIPresentationsHelper from 'ssi-lib/PresentationsHelper';

import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import Toolbar from '@mui/material/Toolbar';

import { Stack, Avatar } from '@mui/material';

import ApplyIcon from '@mui/icons-material/Stars';
import WalletIcon from '@mui/icons-material/CreditCard';
import PhotoIcon from '@mui/icons-material/PhotoSizeSelectActual';

import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';

import ApplyPage from './components/ApplyPage';
import WalletPage from './components/WalletPage';
import PhotoPage from './components/PhotoPage';

import CavendishForm from './components/plugins/cavendish/Form';
import ThodayForm from './components/plugins/thoday/Form';
import FranklinForm from './components/plugins/franklin/Form';

import * as Certs from 'ssi-lib/Certificates';

const image = require('./assets/image.png');
const ledgerImage = require('./assets/banner.png');
const drawerWidth = 90;

export default function App() {

  const api_url = EnvVars.REACT_APP_RUTH_API;
  const hook_url = EnvVars.REACT_APP_RUTH_HOOK;
  const file_server_url = EnvVars.FILE_SERVER_URL;

  const [menuItem, setMenuItem] = useState(0);
  const [applicationStage, setApplicationStage] = useState(0);
  const [invitationJSON, setInvitationJSON] = useState(undefined);

  useEffect(() => {
    if (applicationStage === 0) setInvitationJSON(undefined);
  }, [applicationStage]);

  const [connectionResults, setConnectionResults] = useState([]);
  const [credentialResults, setCredentialResults] = useState([]);
  const [presentationResults, setPresentationResults] = useState([]);
  const [presentationResultsV2, setPresentationResultsV2] = useState([]);

  const [latestConnection, setLatestConnection] = useState([]);
  const [invitationSent, setInvitationSent] = useState(false);

  const [latestPresentationResult, setLatestPresentationResult] = useState(undefined);

  useEffect(SSIConnections.startWebSocketEffect(hook_url, setConnectionResults), []);
  useEffect(SSIConnections.fetchConnectionsEffect(api_url, setConnectionResults), []); // todo *** i think this is getting run twice (see below todo)
  useEffect(SSICredentialExchange.startWebSocketEffect(hook_url, setCredentialResults), []);
  useEffect(SSIPresentationsHelper.startWebSocketEffect(hook_url, setPresentationResults), []); // change "credentialResults" to "activity"
  // ^^^ and have each activity have a type, e.g. "pending credential, credential, presentation", etc... this can be defined in the
  //     websockets... and in the "fetchCredentials" which should be "fetchActivity" ....
  useEffect(SSIPresentationsHelper.startWebSocketEffect_v2(hook_url, setPresentationResultsV2), []);

  // todo *** split this between different ssi-lib helpers and use separate useEffects
  //          to trigger each one; we can update the "type" label in each of those functions
  //          rather than here...
  async function fetchCredentials() {
    let data = await SSICredentialHelper.fetchCredentials(api_url);

    data.pendingCredentials.forEach((x) => x.type = "pending");
    data.storedCredentials.forEach((x) => x.type = "complete");
    data.presentationResults.forEach((x) => x.type = "presentation");
    data.presentationResultsV2.forEach((x) => x.type = "presentation_v2");

    setConnectionResults(data.connectionResults);
    setCredentialResults([...data.pendingCredentials, ...data.storedCredentials]);
    setPresentationResults([...data.presentationResults]);
    setPresentationResultsV2([...data.presentationResultsV2]);
  }

  useEffect(() => {
    fetchCredentials();
  }, []);

  useEffect(async () => {
    if (!invitationSent) return;

    // todo *** we should probably search for the connection associated with the current invite (as with all portals / holder app etc)
    const conn = connectionResults.sort((x, y) => Date.parse(y.created_at) - Date.parse(x.created_at)).at(0); // get the latest connection

    if (!conn) return console.log("Could not obtain the latest result.");

    // todo **** update this so that Ruth has to click trust ping, thus removing need for this franklin check here...
    if (invitationJSON.label !== "franklin") {

      await SSIConnections.trustPing(api_url, conn.connection_id); // send the trust ping automatically
    }
    else console.log("franklin does not require trust ping - that happens on the franklin side !")

    setLatestConnection(conn);
    setInvitationSent(false);
  }, [connectionResults]);

  useEffect(() => {
    var p1, p2;

    if (presentationResults.length > 0) {
      const result = presentationResults[0];
      if (result.state !== "presentation_acked") p1 = result; // presentation was already acknowledged
    }

    if (presentationResultsV2.length > 0) {
      const result = presentationResultsV2[0];
      if (result.state !== "presentation_acked") p2 = result; // presentation was already acknowledged
    }

    if (p1 && p2) {
      if (Date(p1) > Date(p2)) {
        setLatestPresentationResult(p1);
        console.log("presentation v1", p1);
      }
      else {
        setLatestPresentationResult(p2);
        console.log("presentation v2", p2);
      }
    }
    else if (p1) {
      setLatestPresentationResult(p1);
      console.log("presentation v1", p1);
    }
    else if (p2) {
      setLatestPresentationResult(p2);
      console.log("presentation v2", p2);
    }
  }, [presentationResults, presentationResultsV2]);

  return (
    <Box sx={{ display: 'flex' }}>
      <Router>
        <CssBaseline />
        <Box
          component="nav"
          sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
          aria-label="mailbox folders"
        >
          <Drawer
            variant="permanent"
            open
          >
            <Toolbar>
              <Avatar src={image}></Avatar>
            </Toolbar>
            {renderNavButtons()}
          </Drawer>
        </Box>
        {renderRoutes()}
      </Router >
    </Box >
  );

  function renderRoutes() {
    return <Routes>
      <Route
        path="/"
        element={renderPages()}
      />
      <Route
        path="/wallet"
        element={<WalletPage
          credentialResults={credentialResults}
          presentationResults={presentationResults}
          presentationResultsV2={presentationResultsV2}
          api_url={api_url}
        />}
      />
      <Route
        path="/photos"
        element={<PhotoPage />}
      />
    </Routes>
  }

  function renderNavButtons() {
    return <Box m="auto">
      <Stack>
        <Box mt={-13}>
          <Link to="/" onClick={() => {
            setMenuItem(0);
            setApplicationStage(0);
          }}>
            <ApplyIcon style={{ color: menuItem === 0 ? 'black' : 'lightgray' }} />
          </Link>
        </Box>
        <Box mt={6}>
          <Link to="/wallet" onClick={() => { setMenuItem(1) }}>
            <WalletIcon style={{ color: menuItem === 1 ? 'black' : 'lightgray' }} />
          </Link>
        </Box>
        <Box mt={6}>
          <Link to="/photos" onClick={() => { setMenuItem(2) }}>
            <PhotoIcon style={{ color: menuItem === 2 ? 'black' : 'lightgray' }} />
          </Link>
        </Box>
      </Stack>
    </Box>
  }

  function renderPages() {

    return (
      (applicationStage === 0 && <ApplyPage
        drawerWidth={drawerWidth}
        setApplicationStage={setApplicationStage}
        api_url={api_url}
        invitationJSON={invitationJSON}
        setInvitationJSON={setInvitationJSON}
        setInvitationSent={setInvitationSent}
      />) ||
      (applicationStage === 1 && invitationJSON.label === "cavendish" && <CavendishForm
        setApplicationStage={setApplicationStage}
        photographerCertification={Certs.certifications.photographerCertification}
        invitationJSON={invitationJSON}
        setInvitationJSON={setInvitationJSON}
        api_url={api_url}
      />) ||
      (applicationStage === 1 && invitationJSON.label === "thoday" && <ThodayForm
        setApplicationStage={setApplicationStage}
        photographerCertificate={Certs.certifications.photographerCertification}
        imageAuthorshipCertificate={Certs.certifications.imageAuthorshipCertification}
        invitationJSON={invitationJSON}
        setInvitationJSON={setInvitationJSON}
        api_url={api_url}
        credentialResults={credentialResults}
        presentationResults={presentationResults}
        presentationResultsV2={presentationResultsV2}
        file_server_url={file_server_url}
        fetchCredentials={fetchCredentials}
      />) ||
      (applicationStage === 1 && invitationJSON.label === "franklin" && <FranklinForm
        setApplicationStage={setApplicationStage}
        photographerCertificate={Certs.certifications.photographerCertification}
        imageAuthorshipCertificate={Certs.certifications.imageAuthorshipCertification}
        invitationJSON={invitationJSON}
        setInvitationJSON={setInvitationJSON}
        api_url={api_url}
        conn={latestPresentationResult}
        credentialResults={credentialResults}
        presentationResults={presentationResults}
        presentationResultsV2={presentationResultsV2}
        file_server_url={file_server_url}
        fetchCredentials={fetchCredentials}
      />)
    );
  }
}
