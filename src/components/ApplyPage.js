import { React, useState } from 'react';
import { Box } from "@mui/material"

import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Grid from "@mui/material/Grid";

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';

import { Stack } from '@mui/material';

import { Divider } from '@mui/material';
import { Button } from '@mui/material';
import { Chip } from '@mui/material';

import SovDropZone from './SovDropZone';

import InfoIcon from '@mui/icons-material/Info';

const image = require("../assets/banner.png");
const imagePen = require("../assets/pen.png");
const imageVerify = require("../assets/verify.png");

export default function ApplyPage(props) {
    const dummyOrganisations = ["Organisation A", "Organisation B"];

    return <Box
        component="main"
        sx={{ flexGrow: 1, p: 3, width: { sm: `calc(100% - ${props.drawerWidth}px)` }, backgroundColor: "#f9f9f9" }}
    >
        <Grid
            container
            direction="row"
            justifyContent="space-evenly"
            alignItems="stretch"
        >
            <Grid item xs={6}>
                <Box m={3} mt={7}>
                    <Stack direction="row">
                        <Paper variant="none" style={{ backgroundColor: "#f9f9f9" }}>
                            <img style={{ width: 100 }} src={imagePen} />
                        </Paper>
                        <Box ml={3} mt={1}>
                            <Typography variant="h5" sx={{ fontWeight: 'bold' }}>Apply for a credential</Typography>
                            <Typography mt={2}>Apply to receive a press pass or credential for your photos from an organisation.</Typography>
                        </Box>
                    </Stack>
                </Box>
                <Box m={3} mt={15}>
                    <Stack direction="row">
                        <Paper variant="none" style={{ backgroundColor: "#f9f9f9" }}>
                            <img style={{ width: 100 }} src={imageVerify} />
                        </Paper>
                        <Box ml={3} mt={1}>
                            <Typography variant="h5" sx={{ fontWeight: 'bold' }}>Verify credential</Typography>
                            <Typography mt={2}>Organisations can request proof of your credentials and you can share or hide selected information.</Typography>
                        </Box>
                    </Stack>
                </Box>
            </Grid>
            <Grid item xs={6}>
                <Paper elevation={3}>
                    <Box p={7} mb={4}>
                        <Typography variant="h4" style={{ fontWeight: "bold" }}>Get started !</Typography>
                        <Typography mt={4} mb={4}>Visit the website of the organisation and paste the invite below.</Typography>
                        <SovDropZone
                            mt={4}
                            setInvitationSent={props.setInvitationSent}
                            setInvitationJSON={props.setInvitationJSON}
                            api_url={props.api_url}
                        />
                        <Typography mt={4} style={{ fontWeight: "bold" }}>OR</Typography>
                        <Stack direction="row" mt={4} mb={4}>
                            {props.invitationJSON !== undefined && <Box mr={2} style={{ width: "100%" }}>
                                <Chip icon={<InfoIcon />} label={props.invitationJSON ? props.invitationJSON.label.substring(0, 1).toUpperCase() + props.invitationJSON.label.substring(1) + " agent found" : ""}></Chip>
                            </Box>}
                            <Button variant="contained" style={{ backgroundColor: "gray" }}>Paste</Button>
                        </Stack>
                        <Button
                            variant="contained"
                            style={{ width: 200, height: 55, backgroundColor: "blue", fontWeight: "bold" }}
                            onClick={() => { if (props.invitationJSON) props.setApplicationStage(1); }}
                        >CONTINUE</Button>
                    </Box>
                </Paper>
            </Grid>
            <Grid item xs={12}>
                <Box m={3}>
                    <Divider></Divider>
                </Box>
            </Grid>
            <Grid item xs={12}>
                <Box m={3}>
                    <Typography p={1} sx={{ fontWeight: 'bold' }}>Previous organisations</Typography>
                </Box>
                <Box m={3}>
                    <Stack direction="row">
                        {dummyOrganisations.map((name, index) => <Box m={1} key={"org" + index}>
                            <Card sx={{ maxWidth: 345 }}>
                                <CardMedia
                                    component="img"
                                    height="140"
                                    image={image}
                                    alt="green iguana"
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        {name}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Box>
                        )}
                    </Stack>
                </Box>
            </Grid>
        </Grid>
    </Box>
}
