var JSONPretty = require('react-json-pretty');

export default function Details(props) {

    return <JSONPretty data={props.details}></JSONPretty>
}
