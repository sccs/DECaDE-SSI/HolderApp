import { Typography, Box } from '@mui/material';

import { Stack } from '@mui/material';
import { Avatar } from '@mui/material';

import GeneralDetails from '../general-presentation/Details';
import grad from '../grad';

import MailIcon from '@mui/icons-material/Mail';
import SendIcon from '@mui/icons-material/Send';

import * as Certs from 'ssi-lib/Certificates';

export default function Presentation(props) {

    return <Box m={1} borderRadius={4}
        sx={{
            width: 320,
            height: 180,
            background: grad(210, 80, 200),
            '&:hover': {
                backgroundColor: 'white',
                opacity: [0.9, 0.8, 0.7],
            },
        }}
        onClick={() => {
            props.setDetailsOpen(true);
            props.setDetails(GeneralDetails(props))
        }}
    >
        <Box m={1}>
            <Stack direction="row">
                <Box width="30%" m={1}>
                    <Avatar src={props.logo}>{props.icon}</Avatar>
                </Box>
                {!props.pending &&
                    <Box width="40%" mt={1}>
                        <Typography sx={{ fontSize: 12 }} align="right" color="white">Issued</Typography>
                        <Typography sx={{ fontSize: 12 }} align="right" color="white">Valid until</Typography>
                    </Box>}
                {!props.pending && <Box width="5%" />}
                {!props.pending &&
                    <Box width="25%" mt={1}>
                        <Typography sx={{ fontSize: 12, fontWeight: "bold" }} align="left" color="white">14/03/2022</Typography>
                        <Typography sx={{ fontSize: 12, fontWeight: "bold" }} align="left" color="white">14/03/2024</Typography>
                    </Box>}
            </Stack>
            {title(props.details)}
            <Typography ml="3%" mt={1} sx={{ fontSize: 10 }} color="white">{subTitle(props.details)}</Typography>
            {/* <CardActions>
            </CardActions> */}
        </Box>
    </Box>

    function title(x) {
        if (x.state === "proposal_sent")
            return <Stack direction="row" ml="3%" mt="10%" >
                <SendIcon sx={{ color: "white" }} />
                <Typography m={0.5} sx={{ fontSize: 14, fontWeight: "bold" }} color="white">
                    Presentation Proposal Sent
                </Typography></Stack>
        else if (x.presentation)
            return <Stack direction="row" ml="3%" mt="10%" >
                <MailIcon sx={{ color: "white" }} />
                <Typography m={0.5} sx={{ fontSize: 14, fontWeight: "bold" }} color="white">
                    Presentation Received
                </Typography></Stack>
        else return <Stack direction="row" ml="3%" mt="10%" >
            <MailIcon sx={{ color: "white" }} />
            <Typography m={0.5} sx={{ fontSize: 14, fontWeight: "bold" }} color="white">
                Presentation Request Received
            </Typography></Stack>

    }

    function subTitle(x) {
        var source;

        let info = x.presentation_proposal_dict?.presentation_proposal?.attributes;
        if (info && info.length > 0 && info[0].cred_def_id) source = info[0].cred_def_id;

        switch (source) {
            case Certs.certifications.imageAuthorshipCertification.cred_def_id:
                source = "Thoday";
                break;

            case Certs.certifications.photographerCertification.cred_def_id:
                source = "Cavendish";
                break;

            default:
                source = "Unknown";
                break;
        }

        if (x.state === "proposal_sent") return "You sent a presentation proposal of your " + source + " credential to an external agent.";
        else if (x.presentation) return "An external agent responded to your presentaion proposal.";
        else return source + " requested a presentation.";
    }
}
