export default function getParticulars(details) {

    if (details.cred_offer && details.cred_offer.credential_preview) { // a work in progress
        var attributes = details.cred_offer.credential_preview.attributes;

        return {
            geolocation: attributes?.filter((x) => x.name === 'geolocation')[0].value,
            imgHash: attributes?.filter((x) => x.name === 'imgHash')[0].value,
            imgTimestamp: attributes?.filter((x) => x.name === 'imgTimestamp')[0].value,
            imgUrl: attributes?.filter((x) => x.name === 'imgUrl')[0].value,
            macAddress: attributes?.filter((x) => x.name === 'macAddress')[0].value,
        };
    }
    else if (details.attrs) return details.attrs; // a completed credential
    else return { // something went wrong
        geolocation: "unavailable",
        imgHash: "unavailable",
        imgTimestamp: "unavailable",
        imgUrl: "unavailable",
        macAddress: "unavailable",
    };
}
