import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

import React from 'react';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';

import { Chip } from '@mui/material';

import FingerprintIcon from '@mui/icons-material/Fingerprint';
import CancelIcon from '@mui/icons-material/Cancel';

import { useState, useEffect } from 'react';

import { CredentialsView } from '../../CredentialsView';

import { TextField } from '@mui/material';

import * as SSIConnections from 'ssi-lib/Connections';
import * as SSIPresentationsHelper from 'ssi-lib/PresentationsHelper';

import SovDropZoneWithRead from '../../SovDropZoneWithRead';

import * as SSIFileServerHelper from 'ssi-lib/FileServerHelper';

import crypto from "crypto-js";
import random from "random-string-generator";

import dropimage from "../../../assets/dropzone.jpg";

import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

import * as Certs from 'ssi-lib/Certificates';

export default function Form(props) {
    const [value, setValue] = useState("info");

    const [formImageTimeStamp, setFormImageTimeStamp] = useState("");
    const [formCamMacAddress, setFormCamMacAddress] = useState("");
    const [formGeolocation, setFormGeolocation] = useState("");
    const [formThodayCredential, setFormThodayCredential] = useState("");
    const [imageHash, setImageHash] = useState(undefined); // autocomputed
    const [imageUrl, setImageUrl] = useState(undefined); // autocomputed
    const [imageFile, setImageFile] = useState(undefined);
    const [dropImage, setDropImage] = useState(dropimage);

    useEffect(() => props.fetchCredentials(), []);

    useEffect(() => {
        if (imageFile === undefined) return;

        const reader = new FileReader()

        reader.onabort = () => console.log('file reading was aborted')
        reader.onerror = () => console.log('file reading has failed')
        reader.onload = () => {
            let hex = reader.result.split(",")[1];
            let hash = crypto.SHA3(hex);
            hash = hash.toString(crypto.enc.Hex);

            let filename = random(20) + imageFile.name;
            setImageUrl(filename);

            // hex = "file" in webcontroller - the file dump ?
            // hash = "filename" in webcontroller - bit odd as this isn't a hash...
            SSIFileServerHelper.uploadImage(props.file_server_url, imageFile, filename);

            setImageHash(hash);
            setDropImage(reader.result);
        }

        // reader.readAsArrayBuffer(file)
        reader.readAsDataURL(imageFile);

    }, [imageFile]);

    async function submitDetails() {

        // in the webcontroller this is called on clicking "Ruth/Credentials" tab via a useEffect
        let recipientKey = props.invitationJSON.recipientKeys[0];
        let connectionsData = await SSIConnections.getConnections(props.api_url);

        // unlike with the webcontroller, we automatically retrieve the connection record
        let issuerConnectionRecord = connectionsData.find((x) => x.invitation_key === recipientKey);

        // we should obtain one connection:
        if (!issuerConnectionRecord) return console.log("Error retrieving connections.");
        if (issuerConnectionRecord.length == 0) return console.log("Error, no connection was found !");

        // the connection was found !

        let body = {};
        body.connection_id = issuerConnectionRecord.connection_id;

        // as with the original webcontroller we add the fields in a comment field;
        // adding them to attributes (below) does not work - they don't appear at
        // the other end for some reason...
        body.comment = JSON.stringify({
            flag: "ImageCredentialRequest",
            imgHash: imageHash,
            imgUrl: imageUrl,
            imgTimestamp: formImageTimeStamp,
            macAddress: formCamMacAddress,
            geolocation: formGeolocation,
        });

        body.presentation_proposal = {};
        body.presentation_proposal.type = "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/present-proof/1.0/presentation-preview";
        body.auto_present = true;
        body.presentation_proposal.attributes = [
            {
                referent: formThodayCredential.referent,
                cred_def_id: formThodayCredential.cred_def_id,
                name: "isMember",
            },
        ];
        body.presentation_proposal.predicates = [];
        const proposalResponse = await SSIPresentationsHelper.sendPresentationProposal(props.api_url, body);

        setValue("submit");
    }

    return (<Box m="auto">
        <Box
            component="form"
            sx={{ '& .MuiTextField-root': { m: 1, width: '25ch' }, }}
            noValidate
            autoComplete="off"
        >
            <Typography mt={6} mb={5} variant="h4">Thoday Photograph Credential Service</Typography>
            <Tabs
                value={value}
                textColor="primary"
                indicatorColor="primary"
                aria-label="secondary tabs example"
            >
                <Tab value="info" label="Documents" />
                <Tab value="details" label="Metadata" />
                <Tab value="submit" label="All done" />
            </Tabs>
            {value === "info" && <Box>
                <Box mt={2}>
                    <Stack direction="row" mt={5}>
                        <Typography m={1} style={{ fontWeight: "bold" }}>Select one photographer credential</Typography>
                        {formThodayCredential && <Chip icon={<CancelIcon onClick={() => setFormThodayCredential(undefined)}></CancelIcon>} label="Credential selected" />}
                    </Stack>
                    <CredentialsView
                        credentialResults={props.credentialResults}
                        presentationResults={props.presentationResults}
                        presentationResultsV2={props.presentationResultsV2}
                        setCredential={setFormThodayCredential}
                        singleRow
                        disabled={formThodayCredential}
                        filter={(x) => x.cred_def_id === Certs.certifications.photographerCertification.cred_def_id}
                    />
                </Box>
                <Typography mt={5} style={{ fontWeight: "bold" }}>Upload a photograph</Typography>
                <SovDropZoneWithRead
                    dropImage={dropImage}
                    setDropImage={setDropImage}
                    setImageFile={setImageFile}
                ></SovDropZoneWithRead>
                <Box align="center" m="auto">
                    <Button
                        style={{ marginTop: 40, backgroundColor: "blue" }}
                        variant="contained"
                        onClick={() => setValue("details")}>
                        CONTINUE
                    </Button>
                </Box>
            </Box>}
            {value === "details" && <Box>
                <Typography mt={5} style={{ fontWeight: "bold" }}>Your photograph fingerprint</Typography>
                {imageHash && <Box mt={3}>
                    <Chip style={{ margin: 10, width: 500, backgroundColor: 'transparent' }} icon={<FingerprintIcon fontSize="large" />} label={imageHash} >
                    </Chip>
                </Box>}
                <Typography mt={4} style={{ fontWeight: "bold" }}>Enter</Typography>
                <Stack direction="row">
                    <TextField
                        id="standard-required"
                        label="Image Timestamp"
                        variant="standard"
                        onChange={(event) => setFormImageTimeStamp(event.target.value)}
                        value={formImageTimeStamp}
                    />
                    <TextField
                        id="standard-required"
                        label="Camera Mac Address"
                        variant="standard"
                        onChange={(event) => setFormCamMacAddress(event.target.value)}
                        value={formCamMacAddress}
                    />
                    <TextField
                        id="standard-required"
                        label="Geolocation"
                        variant="standard"
                        onChange={(event) => setFormGeolocation(event.target.value)}
                        value={formGeolocation}
                    />
                </Stack>
                <Box align="center" m="auto">
                    <Button
                        style={{ marginTop: 40, backgroundColor: "blue" }}
                        variant="contained"
                        onClick={() => submitDetails()}>
                        CONTINUE
                    </Button>
                </Box>
            </Box>}
            {value === "submit" && <Box>
                <Typography mt={5}>Your submission was sent, please check your wallet for further updates.</Typography>
                <Button
                    style={{ marginTop: 40, marginLeft: 20, backgroundColor: "blue" }}
                    variant="contained"
                    onClick={() => props.setApplicationStage(0)}>
                    CONTINUE
                </Button>
            </Box>}
        </Box>
    </Box>);
}
