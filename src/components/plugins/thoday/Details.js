import Stack from '@mui/material/Stack';

import React from 'react';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';

import LocationOnIcon from '@mui/icons-material/LocationOn';
import FingerprintIcon from '@mui/icons-material/Fingerprint';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import PublicIcon from '@mui/icons-material/Public';
import CameraAltIcon from '@mui/icons-material/CameraAlt';
import { CardMedia } from '@mui/material';
import { Divider } from '@mui/material';

function getParticulars(details) {

    if (details.cred_offer && details.cred_offer.credential_preview) { // a work in progress
        var attributes = details.cred_offer.credential_preview.attributes;

        return {
            geolocation: attributes?.filter((x) => x.name === 'geolocation')[0].value,
            imgHash: attributes?.filter((x) => x.name === 'imgHash')[0].value,
            imgTimestamp: attributes?.filter((x) => x.name === 'imgTimestamp')[0].value,
            imgUrl: attributes?.filter((x) => x.name === 'imgUrl')[0].value,
            macAddress: attributes?.filter((x) => x.name === 'macAddress')[0].value,
        };
    }
    else if (details.attrs) return details.attrs; // a completed credential
    else return { // something went wrong
        geolocation: "unavailable",
        imgHash: "unavailable",
        imgTimestamp: "unavailable",
        imgUrl: "unavailable",
        macAddress: "unavailable",
    };
}

export default function Details(props) {

    var info = getParticulars(props.details);

    return <Box>
        <Typography sx={{ fontWeight: "bold" }}>Thoday Photograph Credentials</Typography>
        <Typography>This credential proves a claim of ownership over a photograph</Typography>
        <Stack mt={4} direction="row">
            <Box width="50%">
                <Typography sx={{ fontWeight: "bold" }}>Issue date</Typography>
                <Typography>--/--/----</Typography>
            </Box>
            <Box>
                <Typography sx={{ fontWeight: "bold" }}>Expiry date</Typography>
                <Typography>--/--/----</Typography>
            </Box>
        </Stack>
        <Box mt={4}>
            <Divider></Divider>
        </Box>
        {info.imgUrl &&
            <CardMedia
                component="img"
                height="140"
                image={info.imgUrl}
                alt="unknown image"
            />}
        <Stack direction="row" mt={4}>
            <LocationOnIcon m={1} />
            <Typography sx={{ marginLeft: 1, fontWeight: "bold" }}>Image geolocation</Typography>
        </Stack>
        <Typography>{info.geolocation}</Typography>

        <Stack direction="row" mt={4}>
            <FingerprintIcon m={1} />
            <Typography sx={{ marginLeft: 1, fontWeight: "bold" }}>Image hash</Typography>
        </Stack>
        <Typography>{info.imgHash}</Typography>

        <Stack direction="row" mt={4}>
            <AccessTimeIcon m={1} />
            <Typography sx={{ marginLeft: 1, fontWeight: "bold" }}>Image timestamp</Typography>
        </Stack>
        <Typography>{info.imgTimestamp}</Typography>

        <Stack direction="row" mt={4}>
            <PublicIcon m={1} />
            <Typography sx={{ marginLeft: 1, fontWeight: "bold" }}>Image url</Typography>
        </Stack>
        <Typography>{info.imgUrl}</Typography>

        <Stack direction="row" mt={4}>
            <CameraAltIcon m={1} />
            <Typography sx={{ marginLeft: 1, fontWeight: "bold" }}>Camera mac address</Typography>
        </Stack>
        <Typography>{info.macAddress}</Typography>
    </Box>
}
