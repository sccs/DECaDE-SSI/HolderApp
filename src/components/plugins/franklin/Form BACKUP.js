import { useState, useEffect } from 'react';
import { Button } from '@mui/material';
import { Typography, Box } from '@mui/material';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

import * as SSIPresentationsHelper from 'ssi-lib/PresentationsHelper';

import { Stack } from '@mui/material';

import { Chip } from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';
import { CredentialsView } from '../../CredentialsView';

export default function Form(props) {
    const [value, setValue] = useState("info");
    const [formFranklinCredential, setFormFranklinCredential] = useState("");

    useEffect(() => props.fetchCredentials(), []);

    const sendImageAuthorshipPresentation = async (e) => {
        e.preventDefault();

        let body = {};
        body.requested_attributes = {
            additionalProp1: {
                cred_id: formFranklinCredential.referent,
                revealed: true,
            },
        };
        body.requested_predicates = {};
        body.self_attested_attributes = {};
        body.trace = true;

        console.log("****************", props.conn.presentation_exchange_id, body, "****************!");

        const presResp = await SSIPresentationsHelper.sendPresentation(
            props.api_url,
            props.conn.presentation_exchange_id,
            body
        );

        setValue("submit")
    };

    return (<Box m="auto">
        <Box
            component="form"
            sx={{ '& .MuiTextField-root': { m: 1, width: '25ch' }, }}
            noValidate
            autoComplete="off"
        >
            <Typography mt={6} mb={5} variant="h4">Franklin News Service</Typography>
            <Tabs
                value={value}
                textColor="primary"
                indicatorColor="primary"
                aria-label="secondary tabs example"
            >
                <Tab value="info" label="Documents" />
                <Tab value="submit" label="All done" />
            </Tabs>
            {value === "info" && <Box>
                <Typography mt={6} variant="h5" style={{ fontWeight: "bold" }}>Your credential</Typography>
                <Box mt={2}>
                    <Stack direction="row" mt={5}>
                        <Typography m={1} style={{ fontWeight: "bold" }}>Select one photograph credential</Typography>
                        {formFranklinCredential && <Chip icon={<CancelIcon onClick={() => setFormFranklinCredential(undefined)}></CancelIcon>} label="Credential selected" />}
                    </Stack>
                    <CredentialsView
                        credentialResults={props.credentialResults}
                        presentationResults={props.presentationResults}
                        presentationResultsV2={props.presentationResultsV2}
                        setCredential={setFormFranklinCredential}
                        singleRow
                        disabled={formFranklinCredential}
                        filter={(x) => x.attrs && x.cred_def_id === props.imageAuthorshipCertificate.cred_def_id}
                    />
                </Box>
                <Box align="center" m="auto">
                    <Button
                        style={{ marginTop: 40, backgroundColor: "blue" }}
                        variant="contained"
                        onClick={(e) => sendImageAuthorshipPresentation(e)}>
                        CONTINUE
                    </Button>
                </Box>
            </Box>}
            {value === "submit" && <Box>
                <Typography mt={5}>Your photograph credential was sent for verification, please check the agents website.</Typography>
                <Button
                    style={{ marginTop: 40, marginLeft: 20, backgroundColor: "blue" }}
                    variant="contained"
                    onClick={() => props.setApplicationStage(0)}>
                    CONTINUE
                </Button>
            </Box>}
        </Box>
    </Box>);
}
