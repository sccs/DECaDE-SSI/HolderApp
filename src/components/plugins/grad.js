import rgb from 'rgb-hex';

export default function grad(r, g, b) {
    if (!r) {
        r = 189;
        g = 189;
        b = 189;
    }
    return "linear-gradient(to bottom, #" + rgb(r, g, b) + " 45%, #" + rgb(0.9 * r, 0.9 * g, 0.9 * b) + " 45%)";
}
