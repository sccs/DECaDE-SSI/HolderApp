import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

import React from 'react';
import Box from '@mui/material/Box';

import * as SSICredentialExchange from 'ssi-lib/CredentialExchange';

import { CardActions } from '@mui/material';

import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CreditCardIcon from '@mui/icons-material/CreditCard';
import Chip from '@mui/material/Chip';

import { CircularProgress, Typography } from "@mui/material";
import { Avatar } from "@mui/material";
import { LinearProgress } from '@mui/material';

import grad from '../grad';
import CavendishDetails from './Details';

export default function Credential(props) {

    var states = getStates(props.details);

    return <Box m={1} borderRadius={4}
        sx={{
            width: 320,
            height: 180,
            background: states.done ? grad(137, 189, 238) : grad(),
            '&:hover': {
                backgroundColor: 'white',
                opacity: states.done && !props.disabled ? [0.9, 0.8, 0.7] : [1.0, 1.0, 1.0],
            },
        }}
        onClick={() => {
            if (states.done && !props.disabled) {
                if (props.setDetailsOpen) props.setDetailsOpen(true);
                if (props.setDetails) props.setDetails(CavendishDetails(props))
                if (props.setCredential) props.setCredential(props.details);
            }
        }}
    >
        <Box m={1}>
            <Stack direction="row">
                <Box width="30%" m={1}>
                    <Avatar src={props.logo}>{props.icon}</Avatar>
                </Box>
                {!props.pending &&
                    <Box width="40%" mt={1}>
                        <Typography sx={{ fontSize: 12 }} align="right" color="white">Issued</Typography>
                        <Typography sx={{ fontSize: 12 }} align="right" color="white">Valid until</Typography>
                    </Box>}
                {!props.pending && <Box width="5%" />}
                {!props.pending &&
                    <Box width="25%" mt={1}>
                        <Typography sx={{ fontSize: 12, fontWeight: "bold" }} align="left" color="white">14/03/2022</Typography>
                        <Typography sx={{ fontSize: 12, fontWeight: "bold" }} align="left" color="white">14/03/2024</Typography>
                    </Box>}
            </Stack>
            {!states.done && <LinearProgress></LinearProgress>}
            <Typography ml="3%" mt="10%" sx={{ fontSize: 14, fontWeight: "bold" }} color="white">Cavendish Photographer Credentials</Typography>
            {props.sub && <Typography ml="3%" mt={1} sx={{ fontSize: 10 }} color="white">You can use this to prove you're a member of Cavendish.</Typography>}
            <CardActions>
                {states.proposal_sent && <Stack direction="row"><CircularProgress /><Typography m={1} color="white">Please wait...</Typography></Stack>}
                {states.offer_received && <Chip label={<Typography color="white">Offer received</Typography>} style={{ borderColor: 'transparent', color: 'black' }} variant="outlined" icon={<CheckCircleIcon style={{ color: 'white' }} />} />}
                {states.offer_received && <Button size="small" onClick={() => SSICredentialExchange.sendCredentialRequest(props.api_url, states.cred_ex_id)}>Accept</Button>}
                {states.offer_received && <Button size="small">Decline</Button>}
                {states.credential_received && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<CheckCircleIcon style={{ color: 'white' }} />} label={<Typography color="white">Credential received</Typography>}></Chip>}
                {states.credential_received && <Button size="small" onClick={() => SSICredentialExchange.storeCredentials(props.api_url, states.cred_ex_id)}>Store</Button>}
                {states.request_sent && <Stack direction="row"><CircularProgress /><Typography m={1} color="white">Awaiting credential</Typography></Stack>}
                {states.done && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<CreditCardIcon style={{ color: 'white' }} />} label={<Typography color="white">Credential stored</Typography>}></Chip>}
            </CardActions>
        </Box>
    </Box>
}

function getStates(details) {

    // a pending credential
    if (details.cred_preview) return {

        cred_ex_id: details.cred_ex_id,
        proposal_sent: details.state === "proposal-sent",
        offer_received: details.state === "offer-received",
        credential_received: details.state === "credential-received",
        request_sent: details.state === "request-sent",
        done: details.state === "done",
        state: details.state,
    };

    // a stored credential
    if (details.attrs) return {

        done: true,
        state: details.state,
    };
}
