import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

import React from 'react';
import Box from '@mui/material/Box';

import * as SSICredentialExchange from 'ssi-lib/CredentialExchange';
import * as SSIConnections from 'ssi-lib/Connections';

import { Typography } from "@mui/material";

import { useState } from 'react';
import { TextField } from '@mui/material';

import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

export default function Form(props) {
    const [value, setValue] = useState("info");
    const [formLegalName, setFormLegalName] = useState("");
    const [formUsername, setFormUsername] = useState("");
    const [formEmployer, setFormEmployer] = useState("");
    const [formInstagramHandle, setFormInstagramHandle] = useState("");

    async function submitDetails() {

        setValue("submit");

        // in the webcontroller this is called on clicking "Ruth/Credentials" tab via a useEffect
        let recipientKey = props.invitationJSON.recipientKeys[0];
        let connectionsData = await SSIConnections.getConnections(props.api_url);

        // unlike with the webcontroller, we automatically retrieve the connection record
        let issuerConnectionRecord = connectionsData.find((x) => x.invitation_key === recipientKey);

        // we should obtain one connection:
        if (!issuerConnectionRecord) return console.log("Error retrieving connections.");
        if (issuerConnectionRecord.length == 0) return console.log("Error, no connection was found !");

        // the connection was found !

        let body = {};
        body.connection_id = issuerConnectionRecord.connection_id;

        body.credential_preview = {};
        body.credential_preview.type = "issue-credential/2.0/credential-preview";

        body.credential_preview.attributes = [
            { name: 'isMember', value: 'true' }, // original webcontroller adds this by default (for cavendish only, not thoday)
            { name: 'legalName', value: formLegalName },
            { name: 'userName', value: formUsername },
            { name: 'employer', value: formEmployer },
            { name: 'instagramHandle', value: formInstagramHandle },
        ];

        body.filter = {};
        body.filter.indy = props.photographerCertification;

        const proposalResponse = await SSICredentialExchange.sendCredentialProposal(props.api_url, body);
    }

    return (<Box m="auto">
        <Box
            component="form"
            sx={{ '& .MuiTextField-root': { m: 1, width: '25ch' }, }}
            noValidate
            autoComplete="off"
        >
            <Typography mt={6} mb={5} variant="h4">Cavendish Photographer Credential Service</Typography>
            <Tabs
                value={value}
                textColor="primary"
                indicatorColor="primary"
                aria-label="secondary tabs example"
            >
                <Tab value="info" label="Personal information" />
                <Tab value="submit" label="All done" />
            </Tabs>
            {value === "info" && <Box>
                <Typography mt={6} variant="h5" style={{ fontWeight: "bold" }}>Personal information</Typography>
                <Typography mt={6} style={{ fontWeight: "bold" }}>Enter</Typography>
                <Stack>
                    <TextField
                        required
                        id="standard-required"
                        label="Legal Name"
                        variant="standard"
                        onChange={(event) => setFormLegalName(event.target.value)}
                        value={props.formLegalName}
                    />
                    <TextField
                        id="standard-required"
                        label="Username"
                        variant="standard"
                        onChange={(event) => setFormUsername(event.target.value)}
                        value={props.formUsername}
                    />
                    <TextField
                        id="standard-required"
                        label="Employer"
                        variant="standard"
                        onChange={(event) => setFormEmployer(event.target.value)}
                        value={props.formEmployer}
                    />
                    <TextField
                        id="standard-required"
                        label="Instagram Handle"
                        variant="standard"
                        onChange={(event) => setFormInstagramHandle(event.target.value)}
                        value={props.formInstagramHandle}
                    />
                </Stack>
                <Box align="center" m="auto">
                    <Button
                        style={{ marginTop: 80, backgroundColor: "blue" }}
                        variant="contained"
                        onClick={() => submitDetails()}>
                        CONTINUE
                    </Button>
                </Box>
            </Box>}
            {value === "submit" && <Box>
                <Typography mt={5}>Your submission was sent, please check your wallet for further updates.</Typography>
                <Button
                    style={{ marginTop: 40, marginLeft: 20, backgroundColor: "blue" }}
                    variant="contained"
                    onClick={() => props.setApplicationStage(0)}>
                    CONTINUE
                </Button>
            </Box>}
        </Box>
    </Box>);
}
