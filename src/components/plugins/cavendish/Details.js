import Stack from '@mui/material/Stack';

import React from 'react';
import Box from '@mui/material/Box';

import WorkIcon from '@mui/icons-material/Work';

import PersonIcon from '@mui/icons-material/Person';
import BadgeIcon from '@mui/icons-material/Badge';
import InstagramIcon from '@mui/icons-material/Instagram';

import { Typography } from "@mui/material";
import { Divider } from '@mui/material';

export default function Details(props) {

    var info = getParticulars(props.details);

    return <Box>
        <Typography sx={{ fontWeight: "bold" }}>Cavendish Photographer Credentials</Typography>
        <Typography>This credential may be used to prove to others that you have been certified as a photographer by Cavendish Credentials.</Typography>
        <Stack mt={4} direction="row">
            <Box width="50%">
                <Typography sx={{ fontWeight: "bold" }}>Issue date</Typography>
                <Typography>--/--/----</Typography>
            </Box>
            <Box>
                <Typography sx={{ fontWeight: "bold" }}>Expiry date</Typography>
                <Typography>--/--/----</Typography>
            </Box>
        </Stack>
        <Box mt={4}>
            <Divider></Divider>
        </Box>
        <Stack direction="row" mt={4}>
            <PersonIcon m={1} />
            <Typography sx={{ marginLeft: 1, fontWeight: "bold" }}>Legal name</Typography>
        </Stack>
        <Typography>{info.legalName}</Typography>

        <Stack direction="row" mt={4}>
            <BadgeIcon m={1} />
            <Typography sx={{ marginLeft: 1, fontWeight: "bold" }}>User name</Typography>
        </Stack>
        <Typography>{info.userName}</Typography>

        <Stack direction="row" mt={4}>
            <WorkIcon m={1} />
            <Typography sx={{ marginLeft: 1, fontWeight: "bold" }}>Employer</Typography>
        </Stack>
        <Typography>{info.employer}</Typography>

        <Stack direction="row" mt={4}>
            <InstagramIcon m={1} />
            <Typography sx={{ marginLeft: 1, fontWeight: "bold" }}>Instagram handle</Typography>
        </Stack>
        <Typography>{info.instagramHandle}</Typography>
    </Box>
}

function getParticulars(details) {

    // a pending credential
    if (details.cred_preview) {

        var attributes = details.cred_preview?.attributes
        return {

            legalName: attributes?.filter((x) => x.name === 'legalName')[0].value,
            userName: attributes?.filter((x) => x.name === 'userName')[0].value,
            employer: attributes?.filter((x) => x.name === 'employer')[0].value,
            instagramHandle: attributes?.filter((x) => x.name === 'instagramHandle')[0].value,
            state: details.state,
        };
    }

    // a stored credential
    if (details.attrs) {

        var attributes = details.attrs;
        return {

            legalName: attributes.legalName,
            userName: attributes.userName,
            employer: attributes.employer,
            instagramHandle: attributes.instagramHandle,
            state: details.state,
        };
    }
}
