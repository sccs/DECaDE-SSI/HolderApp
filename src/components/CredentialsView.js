import * as React from 'react';

import { ImageList } from "@mui/material";
import { ImageListItem } from "@mui/material";

import CavendishCredential from './plugins/cavendish/Credential';
import ThodayCredential from './plugins/thoday/Credential';
import GeneralPresentation from './plugins/general-presentation/Presentation';
import UnknownCredential from './plugins/general-unknown/Credential';

import { Paper } from '@mui/material';
import { Stack } from '@mui/material';

import * as Certs from 'ssi-lib/Certificates';

export function CredentialsView(props) {

    const [cardList, setCardList] = React.useState([]);

    React.useEffect(() => {

        var list = [];

        var counter = 0;
        function getCounter() {
            return counter++;
        }

        // credentials interactions
        props.credentialResults.forEach((x) => {

            if (props.filter && !props.filter(x)) return; // apply any bespoke filtering first

            // check for cavendish
            if (x.cred_def_id === Certs.certifications.photographerCertification.cred_def_id ||
                x?.by_format?.cred_offer?.indy?.cred_def_id === Certs.certifications.photographerCertification.cred_def_id) {

                list.push(<CavendishCredential
                    key={getCounter()}
                    details={x}
                    api_url={props.api_url}
                    setDetailsOpen={props.setDetailsOpen}
                    setDetails={props.setDetails}
                    setCredential={props.setCredential}
                    disabled={props.disabled}
                />);
            }
            // check for thoday
            else if (x.cred_def_id === Certs.certifications.imageAuthorshipCertification.cred_def_id ||
                x?.by_format?.cred_offer?.indy?.cred_def_id === Certs.certifications.imageAuthorshipCertification.cred_def_id) {

                list.push(<ThodayCredential
                    key={getCounter()}
                    details={x}
                    api_url={props.api_url}
                    setDetailsOpen={props.setDetailsOpen}
                    setDetails={props.setDetails}
                    setCredential={props.setCredential}
                    disabled={props.disabled}
                />);
            }
            // some messages come in as undefined
            else {

                list.push(<UnknownCredential
                    key={getCounter()}
                    details={x}
                    api_url={props.api_url}
                    setDetailsOpen={props.setDetailsOpen}
                    setDetails={props.setDetails}
                    setCredential={props.setCredential}
                    disabled={props.disabled}
                />);
            }
        });

        // presentations
        props.presentationResults.forEach((x) => {

            if (props.filter && !props.filter(x)) return;

            list.push(<GeneralPresentation
                key={getCounter()}
                details={x}
                setDetailsOpen={props.setDetailsOpen}
                setDetails={props.setDetails}
            />);
        });

        props.presentationResultsV2.forEach((x) => {

            if (props.filter && !props.filter(x)) return;

            list.push(<GeneralPresentation
                key={getCounter()}
                details={x}
                setDetailsOpen={props.setDetailsOpen}
                setDetails={props.setDetails}
            />);
        });

        setCardList(list);

    }, [props.credentialResults, props.presentationResults, props.presentationResultsV2]);

    if (props.singleRow) return <Paper style={{ overflow: 'auto', maxWidth: 800, opacity: props.disabled ? 0.25 : 1 }} elevation={0}>
        <Stack direction="row">
            {cardList.map((item, index) => (
                <ImageListItem key={"cred" + index}>
                    {item}
                </ImageListItem>
            ))}
        </Stack>
    </Paper>

    if (!props.singleRow) return <ImageList sx={{ width: "100%", height: "100%" }} cols={4}>
        {cardList.map((item, index) => (
            <ImageListItem key={"cred" + index}>
                {item}
            </ImageListItem>
        ))}
    </ImageList>
}
