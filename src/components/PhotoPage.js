import { React } from 'react';
import { Box } from "@mui/material"

import Typography from '@mui/material/Typography';
import { Divider } from '@mui/material';

import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';

import { Card, CardMedia } from '@mui/material';

const itemData = [
    {
        img: 'https://images.unsplash.com/photo-1551963831-b3b1ca40c98e',
        title: 'Breakfast',
    },
    {
        img: 'https://images.unsplash.com/photo-1551782450-a2132b4ba21d',
        title: 'Burger',
    },
    {
        img: 'https://images.unsplash.com/photo-1522770179533-24471fcdba45',
        title: 'Camera',
    },
    {
        img: 'https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c',
        title: 'Coffee',
    },
    {
        img: 'https://images.unsplash.com/photo-1533827432537-70133748f5c8',
        title: 'Hats',
    },
    {
        img: 'https://images.unsplash.com/photo-1558642452-9d2a7deb7f62',
        title: 'Honey',
    },
    {
        img: 'https://images.unsplash.com/photo-1516802273409-68526ee1bdd6',
        title: 'Basketball',
    },
    {
        img: 'https://images.unsplash.com/photo-1518756131217-31eb79b20e8f',
        title: 'Fern',
    },
    {
        img: 'https://images.unsplash.com/photo-1597645587822-e99fa5d45d25',
        title: 'Mushrooms',
    },
    {
        img: 'https://images.unsplash.com/photo-1567306301408-9b74779a11af',
        title: 'Tomato basil',
    },
    {
        img: 'https://images.unsplash.com/photo-1471357674240-e1a485acb3e1',
        title: 'Sea star',
    },
    {
        img: 'https://images.unsplash.com/photo-1589118949245-7d38baf380d6',
        title: 'Bike',
    },
];

export default function WalletPage(props) {

    return <Box
        component="main"
        sx={{ flexGrow: 1, p: 3, width: { sm: `calc(100% - ${props.drawerWidth}px)` } }}
    >
        <Typography m={2} variant="h5" style={{ fontWeight: "bold" }}>Photos</Typography>
        <Typography m={2} mt={4} >Your photo library</Typography>
        <Divider></Divider>
        <Box mt={3}>
            <ImageList sx={{ width: "100%", height: "100%" }} cols={5} rowHeight={230}>
                {itemData.map((item) => (
                    <ImageListItem key={item.img}>

                        <Card sx={{ maxWidth: 320 }}>
                            <CardMedia
                                component="img"
                                height="200"
                                image={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                                alt="Credential"
                            />
                        </Card>
                    </ImageListItem>
                ))}
            </ImageList>
        </Box>
    </Box>
}
