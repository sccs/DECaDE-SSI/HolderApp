import { React, useState } from 'react';
import { Box } from "@mui/material"

import Typography from '@mui/material/Typography';
import Grid from "@mui/material/Grid";
import { Stack } from '@mui/material';

import { Divider } from '@mui/material';

import { Chip } from '@mui/material';

import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import { Drawer } from '@mui/material';

import { DialogTitle } from '@mui/material';
import { IconButton } from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';

import { CredentialsView } from './CredentialsView';

export default function WalletPage(props) {
    const [status, setStatus] = useState("all");
    const [chipPhotoCred, setChipPhotoCred] = useState(false);
    const [chipPressPass, setChipPressPass] = useState(false);

    const [detailsOpen, setDetailsOpen] = useState(false);
    const [details, setDetails] = useState(<div />);

    return <Box
        component="main"
        sx={{ flexGrow: 1, p: 3, width: { sm: `calc(100% - ${props.drawerWidth}px)` } }}
    >
        <Drawer
            anchor="right"
            open={detailsOpen}
        >
            <Box width={400}>
                <DialogTitle disableTypography className="drawerTitle">
                    <Stack direction="row">
                        Details
                        <Box flex={1}></Box>
                        <IconButton onClick={() => { setDetailsOpen(false) }}>
                            <CancelIcon />
                        </IconButton>
                    </Stack>
                </DialogTitle>
                <Box m={3}>
                    {details}
                </Box>
            </Box>
        </Drawer>
        <Typography m={2} variant="h5" style={{ fontWeight: "bold" }}>Wallet</Typography>
        <Box>
            <Grid container>
                <Grid item xs={4}>
                    <Typography m={2} >Digital wallet containing your credentials</Typography>
                </Grid>
                <Grid item xs={8}>

                    <Stack
                        direction="row"
                        justifyContent="right"
                        alignItems="baseline"
                        spacing={2}
                    >
                        <Typography m={2} style={{ fontWeight: "bold" }}>Type</Typography>
                        <Chip
                            style={{ margin: 10 }}
                            variant={chipPhotoCred ? "contained" : "outlined"}
                            label="Photo credential"
                            onClick={() => setChipPhotoCred(!chipPhotoCred)}
                            color="primary"
                        ></Chip>
                        <Chip
                            style={{ margin: 10 }}
                            variant={chipPressPass ? "contained" : "outlined"}
                            label="Press pass"
                            onClick={() => setChipPressPass(!chipPressPass)}
                            color="primary"
                        ></Chip>
                        <Typography m={2} ml={6} style={{ fontWeight: "bold" }}>Status</Typography>
                        <Box sx={{ minWidth: 120 }}>
                            <FormControl fullWidth variant="standard">
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={status}
                                // onChange={handleChange}
                                >
                                    <MenuItem value="all">All</MenuItem>
                                    <MenuItem value="pending">Pending</MenuItem>
                                    <MenuItem value="decision">Decision</MenuItem>
                                    <MenuItem value="storage">Storage</MenuItem>
                                </Select>
                            </FormControl>
                        </Box>
                    </Stack>
                </Grid>
            </Grid>
        </Box>
        <Divider></Divider>
        <Box mt={3}>
            <CredentialsView
                credentialResults={props.credentialResults}
                presentationResults={props.presentationResults}
                presentationResultsV2={props.presentationResultsV2}
                setDetails={setDetails}
                setDetailsOpen={setDetailsOpen}
                api_url={props.api_url}
            />
        </Box>
    </Box>
}
