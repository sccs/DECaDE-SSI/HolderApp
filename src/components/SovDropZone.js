import React from 'react'
import Dropzone from 'react-dropzone'
import * as SSIConnections from 'ssi-lib/Connections';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

import { Box } from '@mui/material';

// not sure why, but we have to wrap the <Dropzone> in a <div>
// whose onDrop is set to the function we want calling. setting
// onDrop directly in <Dropzone> doesn't work. perhaps this
// is cross-browser drag-and-drop specific...

export default function SovDropZone(props) {

  async function drop(event) {

    event.preventDefault();
    var data;

    try {
      data = JSON.parse(event.dataTransfer.getData('text'));
      props.setInvitationJSON(data);
    } catch (e) {
      console.log("error parsing data.");
      // If the text data isn't parsable we'll just ignore it.
      return;
    }

    // Do something with the data
    let aliasRaw = "";
    let autoAccept = true;
    let mediationIdRaw = "";
    let bodyRaw = data;

    let storeFlag = await SSIConnections.receiveInvitation(props.api_url, aliasRaw, autoAccept, mediationIdRaw, bodyRaw)
    console.log("invite --> ", data);

    if (!storeFlag.status) console.log("Receive invitation error:", storeFlag.error);

    props.setInvitationSent(true);
  }

  return (
    <div onDrop={drop}>
      <Dropzone>
        {() => (
          <Card elevation={0}>
            <CardActionArea>
              <CardContent style={{ backgroundColor: "#f9f9f9" }}>
                <Box>
                  <Typography m={4} align="center" component="div" >
                    Drag & drop QR code
                  </Typography>
                </Box>
              </CardContent>
            </CardActionArea>
          </Card>
        )}
      </Dropzone>
    </div>
  );
}
