import * as React from 'react';
import { useState } from 'react';
import { BottomNavigation, BottomNavigationAction } from '@mui/material';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import ContentCopyOutlinedIcon from '@mui/icons-material/ContentCopyOutlined';
import AlternateEmailOutlinedIcon from '@mui/icons-material/AlternateEmailOutlined';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';

const SovBottomNav = (props) => {
  const [value, setValue] = useState(0);

  return (
    <BottomNavigation
      style={{ width: '100%', position: 'fixed', bottom: 0 }}
      showLabels
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
    >
      <BottomNavigationAction label="Home" icon={<HomeOutlinedIcon />} />
      <BottomNavigationAction label="Account" icon={<AccountCircleOutlinedIcon />} />
      <BottomNavigationAction label="Docs" icon={<ContentCopyOutlinedIcon />} />
      <BottomNavigationAction label="Agents" icon={<AlternateEmailOutlinedIcon />} />
      <BottomNavigationAction label="Messages" icon={<EmailOutlinedIcon />} />
    </BottomNavigation>
  );
}

export default SovBottomNav;
